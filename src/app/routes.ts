import {Routes} from '@angular/router';
import {ECommerceComponent} from './e-commerce/e-commerce.component';
import {PageNotFoundComponent} from './shared/page-not-found/page-not-found.component';
import {DashboardComponent} from './dashboard/dashboard.component';

export const ROUTES: Routes = [
  {
    path: 'e-commerce',
    component: ECommerceComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];
