import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ECommerceTopBarComponent } from './e-commerce-top-bar.component';

describe('ECommerceTopBarComponent', () => {
  let component: ECommerceTopBarComponent;
  let fixture: ComponentFixture<ECommerceTopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ECommerceTopBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ECommerceTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
