import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../product.model';

@Component({
  selector: 'app-e-commerce-product-alerts',
  templateUrl: './e-commerce-product-alerts.component.html',
  styleUrls: ['./e-commerce-product-alerts.component.scss']
})
export class ECommerceProductAlertsComponent implements OnInit {
  @Input() product: Product;
  @Output() notify = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
