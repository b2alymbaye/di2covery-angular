import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ECommerceProductAlertsComponent } from './e-commerce-product-alerts.component';

describe('ECommerceProductAlertsComponent', () => {
  let component: ECommerceProductAlertsComponent;
  let fixture: ComponentFixture<ECommerceProductAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ECommerceProductAlertsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ECommerceProductAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
