import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ECommerceProductPreviewComponent } from './e-commerce-product-preview.component';

describe('ECommerceProductPreviewComponent', () => {
  let component: ECommerceProductPreviewComponent;
  let fixture: ComponentFixture<ECommerceProductPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ECommerceProductPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ECommerceProductPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
