import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../product.model';

@Component({
  selector: 'app-e-commerce-product-preview',
  templateUrl: './e-commerce-product-preview.component.html',
  styleUrls: ['./e-commerce-product-preview.component.scss']
})
export class ECommerceProductPreviewComponent implements OnInit {
  @Input() product: Product;   // Communication top to bottom.
  @Output() share = new EventEmitter();   // Communication bottom to top.
  @Output() notify = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}
