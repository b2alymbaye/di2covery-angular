import {Component, OnInit} from '@angular/core';
import {Product} from '../product.model';
import {productData} from '../product-data';

@Component({
  selector: 'app-e-commerce-product-list',
  templateUrl: './e-commerce-product-list.component.html',
  styleUrls: ['./e-commerce-product-list.component.scss']
})
export class ECommerceProductListComponent implements OnInit {
  products: Array<Product>;

  constructor() { }

  ngOnInit() {
    this.products = productData;   // productData is a set of predefine product.
  }

  onShare() {
    window.alert('The product has been shared!');
  }

  onNotify() {
    window.alert('You will be notified when the product goes on sale');
  }
}
