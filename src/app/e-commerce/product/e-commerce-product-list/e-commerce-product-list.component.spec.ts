import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ECommerceProductListComponent } from './e-commerce-product-list.component';

describe('ECommerceProductListComponent', () => {
  let component: ECommerceProductListComponent;
  let fixture: ComponentFixture<ECommerceProductListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ECommerceProductListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ECommerceProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
