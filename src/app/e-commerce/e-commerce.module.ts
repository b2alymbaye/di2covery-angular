import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ECommerceTopBarComponent} from './e-commerce-top-bar/e-commerce-top-bar.component';
import {ECommerceProductListComponent} from './product/e-commerce-product-list/e-commerce-product-list.component';
import {ECommerceProductPreviewComponent} from './product/e-commerce-product-preview/e-commerce-product-preview.component';
import {ECommerceComponent} from './e-commerce.component';
import {ECommerceProductAlertsComponent} from './product/e-commerce-product-alerts/e-commerce-product-alerts.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [
    ECommerceTopBarComponent,
    ECommerceProductListComponent,
    ECommerceProductPreviewComponent,
    ECommerceComponent,
    ECommerceProductAlertsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class ECommerceModule { }
